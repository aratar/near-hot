dev_console:
	docker-compose exec php bash
generate_migrations:
	docker-compose exec php bash -c 'php bin/console doctrine:migrations:diff'
assets_install:
	docker-compose exec php bash -c 'php bin/console assets:install'
composer_install:
	docker-compose exec php bash -c 'composer install'
migrate:
	docker-compose exec php bash -c 'php bin/console doctrine:migrations:migrate -n'
rollback: # example: make rollback step=1
	docker-compose exec php bash -c 'php bin/console doctrine:migrations:migrate current-$(step)'