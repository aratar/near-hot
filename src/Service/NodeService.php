<?php

namespace App\Service;

use GuzzleHttp\Client;

class NodeService
{
  public function __construct(private Client $client)
  {
  }

  public function claim(array $data) {
      $resp = $this->client->request('POST', 'http://nodejs:3001/call',[
          'body' => json_encode($data)
      ]);

      return $resp;
  }
}