<?php

namespace App\Service;

use App\Dto\ClaimDto;
use App\Entity\Account;
use App\Repository\AccountRepository;
use App\Repository\TokenRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use RuntimeException;
use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Lock\Store\FlockStore;

class ClaimService
{
    public function __construct(
        private NodeService $nodeService,
        private TokenRepository $tokenRepository,
        private AccountRepository $accountRepository,
        private EntityManagerInterface $entityManager,
    )
    {
    }

    public function claim(string $token, ClaimDto $claimDto): bool  {
        $tokenEntity = $this->tokenRepository->findOneBy(['token' => $token]);

        if (!$tokenEntity) {
            throw new RuntimeException('incorrect token');
        }

        $expirationDate = $tokenEntity->getExpirationDate();

        if ($expirationDate < new DateTimeImmutable()) {
            throw new TokenExpiredException('token is expired');
        }

        $accountEntity = $this
            ->accountRepository->findOneBy(
                ['token' => $tokenEntity, 'name' => $claimDto->accountId]
            );

        if ($accountEntity) {
            return $this->claimFromNode($claimDto);
        }

        $store = new FlockStore('var/locks');
        $factory = new LockFactory($store);

        $lock = $factory->createLock('accounts-' . $tokenEntity->getId());

        if ($lock->acquire(true)) {
            $accountEntityCount =
                $this->accountRepository->count(
                        ['token' => $tokenEntity]
                    );

            $canAddNew = $tokenEntity->getAccountCount() > $accountEntityCount;

            if (!$canAddNew) {
                $lock->release();
                throw new RuntimeException('Account limit = ' . $tokenEntity->getAccountCount());
            }

            $acc = new Account();
            $acc->setName($claimDto->accountId);
            $acc->setToken($tokenEntity);
            $acc->setCreatedAt(new DateTimeImmutable());
            $this->entityManager->persist($acc);

            $claimFromNode = $this->claimFromNode($claimDto);

            if ($claimFromNode) {
                $this->entityManager->flush();
            }

            $lock->release();
        }

        return $claimFromNode;
    }

    /**
     * @param ClaimDto $claimDto
     *
     * @return true
     * @throws \JsonException
     */
    private function claimFromNode(ClaimDto $claimDto): bool
    {
        $resp = $this->nodeService->claim([
            'account_id' => $claimDto->accountId,
            'private_key' => $claimDto->privateKey,
            'contract' => "game.hot.tg",
            'method' => "claim",
            'attached_gas' => "100000000000000",
            'attached_tokens' => "0",
        ]);
        $contents = $resp->getBody()->getContents();
        $respJson = json_decode($contents, true, 512, JSON_THROW_ON_ERROR);

        $successValue = $respJson['status']['SuccessValue'] ?? null;

        if ($successValue === null || $resp->getStatusCode() !== 200) {
            throw new RuntimeException($contents);
        }

        return true;
    }
}