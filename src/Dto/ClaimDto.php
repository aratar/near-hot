<?php

namespace App\Dto;

use Doctrine\DBAL\Types\Types;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class ClaimDto
{
    #[NotBlank]
    #[Length(min: 3, max: 200)]
    #[Type(Types::STRING)]
    public string $accountId;

    #[NotBlank]
    #[Length(min: 3, max: 200)]
    #[Type(Types::STRING)]
    public string $privateKey;
}