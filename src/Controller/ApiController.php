<?php

namespace App\Controller;

namespace App\Controller;

use App\Dto\ClaimDto;
use App\Service\ClaimService;
use App\Service\TokenExpiredException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;
use Throwable;

class ApiController extends AbstractController
{
    public function __construct(private ClaimService $claimService, protected RequestStack $requestStack,)
    {
    }

    #[Route('/api/claim', methods: ["POST"])]
    public function claim(
        #[MapRequestPayload] ClaimDto $claimDto,
    ): Response
    {
        $request = $this->requestStack->getCurrentRequest();
        $token = $request?->headers->get('Token');

        if (!$token) {
            return $this->json(['status' => 403, 'detail' => 'Token needed'], 403);
        }

        try {
            $this->claimService->claim($token, $claimDto);
        } catch (TokenExpiredException $e) {
            return $this->json(['status' => 401, 'detail' => $e->getMessage()], 401);
        } catch (Throwable $e) {
            return $this->json(['status' => 500, 'detail' => $e->getMessage()], 500);
        }

        return $this->json(['status' => 200]);
    }
}